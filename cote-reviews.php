<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              https://cote-reviews.com
 * @since             1.0.0
 * @package           Cote_Reviews
 *
 * @wordpress-plugin
 * Plugin Name:       Cote Reviews
 * Plugin URI:        https://cote-reviews.com
 * Description:       A plugin that allows you to review.
 * Version:           1.0.0
 * Author:            Tapha
 * Author URI:        https://cote-reviews.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       cote-reviews
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'COTE_REVIEWS_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-cote-reviews-activator.php
 */
function activate_cote_reviews() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cote-reviews-activator.php';
	Cote_Reviews_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-cote-reviews-deactivator.php
 */
function deactivate_cote_reviews() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-cote-reviews-deactivator.php';
	Cote_Reviews_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_cote_reviews' );
register_deactivation_hook( __FILE__, 'deactivate_cote_reviews' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-cote-reviews.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_cote_reviews() {

	/**
	 * Check if ACF is active
	 **/
	if ( in_array( 'advanced-custom-fields/acf.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	    // Run the plugin
	    $plugin = new Cote_Reviews();
		$plugin->run();
	}

}
run_cote_reviews();
