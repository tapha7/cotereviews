<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://cote-reviews.com
 * @since      1.0.0
 *
 * @package    Cote_Reviews
 * @subpackage Cote_Reviews/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Cote_Reviews
 * @subpackage Cote_Reviews/admin
 * @author     Tapha <taphangum7@gmail.com>
 */
class Cote_Reviews_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	private function add_featured_image_support() {
		add_theme_support('post-thumbnails');
    	add_post_type_support( 'cote_review', 'thumbnail' );
	}

	public function cote_review_custom_post_type() {
		$this->add_featured_image_support();
		register_post_type('cote_review',
			array(
				'labels'      => array(
					'name'          => __('Cote Reviews', 'textdomain'),
					'singular_name' => __('Review', 'textdomain'),
				),
					'public'      => true,
					'has_archive' => true,
					'show_in_menu'=> true,
					'menu_position'=> 6
			)
		);
	}

	public function cote_register_taxonomy_review() {
     $labels = array(
         'name'              => _x( 'Review Tags', 'taxonomy general name' ),
         'singular_name'     => _x( 'Course', 'taxonomy singular name' ),
         'search_items'      => __( 'Search Review Tags' ),
         'all_items'         => __( 'All Review Tags' ),
         'parent_item'       => __( 'Parent Review' ),
         'parent_item_colon' => __( 'Parent Review:' ),
         'edit_item'         => __( 'Edit Review Tag' ),
         'update_item'       => __( 'Update Review Tag' ),
         'add_new_item'      => __( 'Add New Review Tag' ),
         'new_item_name'     => __( 'New Review Tag Name' ),
         'menu_name'         => __( 'Review Tags' ),
     );
     $args   = array(
         'hierarchical'      => true, // make it hierarchical (like categories)
         'labels'            => $labels,
         'show_ui'           => true,
         'show_admin_column' => true,
         'query_var'         => true,
         'rewrite'           => [ 'slug' => 'review_tags' ],
     );
     register_taxonomy( 'review_tags', [ 'cote_review' ], $args );
	}

	public function default_review_posts_private() {
		if ( $post->post_type == 'cote_review' && $new_status == 'publish' && $old_status  != $new_status ) {
			$post->post_status = 'private';
			wp_update_post( $post );
		}
	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Cote_Reviews_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Cote_Reviews_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/cote-reviews-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Cote_Reviews_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Cote_Reviews_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/cote-reviews-admin.js', array( 'jquery' ), $this->version, false );

	}

}
