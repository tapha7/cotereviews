<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://cote-reviews.com
 * @since      1.0.0
 *
 * @package    Cote_Reviews
 * @subpackage Cote_Reviews/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
